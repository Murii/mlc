/*
   Copyright (c) 2018 Muresan Vlad Mihail
   Contact Info muresanvladmihail@gmail.com, murii@tilde.team

   Permission is granted to anyone to use this software for any purpose,
   including commercial applications, and to alter it and redistribute it
   freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software.
   Shall you use this software in a product, an acknowledgment and the
   contact info(if there is any) of the author(s) must be placed in
   the product documentation.
   2. This notice may not be removed or altered from any source distribution.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
   BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE
   AND NON-INFRINGEMENT. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE
   BE LIABLE FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE, ARISING
   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
   */

#ifndef MLC_H
#define MLC_H

#include <stdio.h>

/*
 * 0.1.2 - changed license. If #MLC_ACT_NORMAL, if defined it will make mlc work like
	normal malloc/calloc etc
 * 0.1.0 - initial commit
 * 0.1.1 - better management & performance
*/
#define MLC_VERSION 0.1.2

#define mlc_malloc(size) _mlc_malloc(size, __FILE__, __LINE__)
#define mlc_calloc(nitems, size) _mlc_calloc(nitems, size, __FILE__, __LINE__)
#define mlc_realloc(ptr, size) _mlc_realloc(ptr, size, __FILE__, __LINE__)
#define mlc_size(ptr) _mlc_size(ptr, __FILE__, __LINE__)
#define mlc_free(ptr) _mlc_free(ptr, __FILE__, __LINE__)

void mlc_init();
void mlc_destroy();
void* _mlc_malloc(size_t size, const char* file, unsigned line);
void* _mlc_calloc(size_t nitems, size_t size, const char* file, unsigned line);
void* _mlc_realloc(void* ptr, size_t size, const char* file, unsigned line);
void _mlc_free(void* ptr, const char* file, unsigned line);
size_t _mlc_size(void* ptr, const char* file, unsigned line);
size_t mlc_usage();
void mlc_dump(FILE* file);

#endif

