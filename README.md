About
====
mlc(memory leak counter) is wrote in C89 and it's not thread safe. 
It is used mostly in debbuging when you want to know if your C/C++ application has memory leaks, how many, what line and what file(s).

Usage
=======
```
#include "mlc.h"

#define MLC_CHECK_FOR_ERRORS

int main(void) {

    mlc_init();

    char *a = mlc_malloc(126);
    char *b = mlc_calloc(2, 2);
    char *c = mlc_malloc(128);

    a = mlc_realloc(a, 64);

    printf("Current memory usage: %zu bytes\n", mlc_usage());
    printf("Size of 'a' variable's allocation: %zu bytes\n", mlc_size(a));

    mlc_free(c);
    mlc_free(a);

    mlc_dump(stdout);

    mlc_destroy();
    printf("done\n");
    return 0;
}
```

Authors
=======
Murii (Muresan Vlad Mihail)

License 
=======
   Copyright (c) 2018 Muresan Vlad Mihail
   Contact Info muresanvladmihail@gmail.com, murii@tilde.team

   Permission is granted to anyone to use this software for any purpose,
   including commercial applications, and to alter it and redistribute it
   freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software.
   Shall you use this software in a product, an acknowledgment and the
   contact info(if there is any) of the author(s) must be placed in
   the product documentation.
   2. This notice may not be removed or altered from any source distribution.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
   BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE
   AND NON-INFRINGEMENT. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE
   BE LIABLE FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE, ARISING
   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


